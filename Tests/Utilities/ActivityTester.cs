﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace OneOfZero.DotMasquerade.Tests.Utilities
{
	public abstract class ActivityTester
	{
		/// <summary>
		/// Holds a log of activities
		/// </summary>
		protected List<Activity> _activityLog;

		[SetUp]
		public void IntializeTest()
		{
			this._activityLog = new List<Activity>();
		}

		public void ResetLog()
		{
			this.IntializeTest();
		}

		/// <summary>
		/// Logs the provided <paramref name="activity"/>
		/// </summary>
		/// <param name="activity">Activity to log</param>
		/// <returns>The logged activity</returns>
		public Activity LogActivity(Activity activity)
		{
			this._activityLog.Add(activity);
			return activity;
		}

		/// <summary>
		/// Asserts that the <paramref name="reference"/> activity was logged
		/// </summary>
		/// <param name="reference">Activity</param>
		/// <param name="target">Activity</param>
		public void AssertLogged(Activity reference)
		{
			Assert.Contains(reference, this._activityLog);
		}

		/// <summary>
		/// Asserts that the <paramref name="reference"/> activity was not logged
		/// </summary>
		/// <param name="reference">Activity</param>
		/// <param name="target">Activity</param>
		public void AssertNotLogged(Activity reference)
		{
			Assert.False(this._activityLog.Contains(reference));
		}

		/// <summary>
		/// Asserts that the <paramref name="reference"/> activity was logged before the 
		/// <paramref name="target"/> activity
		/// </summary>
		/// <param name="reference">Activity</param>
		/// <param name="target">Activity</param>
		public void AssertLoggedBefore(Activity reference, Activity target)
		{
			Assert.Contains(reference, this._activityLog);
			Assert.Contains(target, this._activityLog);

			Assert.Less(
				this._activityLog.IndexOf(reference),
				this._activityLog.IndexOf(target),
				"Activity '{0}' was logged after activity '{1}'",
				reference.Name,
				target.Name
			);
		}
	}
}
