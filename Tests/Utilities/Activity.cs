﻿using System;

namespace OneOfZero.DotMasquerade.Tests.Utilities
{
	public class Activity
	{
		/// <summary>
		/// Name of the activity
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Timestamp for the activity
		/// </summary>
		public DateTime Timestamp { get; set; }

		/// <summary>
		/// Initializes an activity object with the provided <paramref name="name"/> and optional 
		/// timestamp
		/// </summary>
		/// <param name="name">Name of the activity</param>
		public Activity(string name)
		{
			this.Name = name;
			this.Timestamp = DateTime.Now;
		}

		/// <summary>
		/// Initializes an activity object with the provided <paramref name="name"/> and optional
		/// <paramref name="timestamp"/>
		/// </summary>
		/// <param name="name">Name of the activity</param>
		/// <param name="timestamp">Timestamp of the activity</param>
		public Activity(string name, DateTime timestamp)
		{
			this.Name = name;
			this.Timestamp = timestamp;
		}

		public override int GetHashCode()
		{
			return this.Name.GetHashCode();
		}

		public override string ToString()
		{
			return String.Format("{0}: {1}",
				(Int32)(this.Timestamp.Subtract(new DateTime(1970, 1, 1))).TotalSeconds,
				this.Name
			);
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != this.GetType())
			{
				return false;
			}

			Activity activity = obj as Activity;
			return this.Name == activity.Name;
		}

		public static bool operator ==(Activity a, Activity b)
		{
			return a != null && b != null && a.Equals(b);
		}

		public static bool operator !=(Activity a, Activity b)
		{
			return !(a == b);
		}
	}
}
