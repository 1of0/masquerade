﻿using System;
using NUnit.Framework;
using OneOfZero.DotMasquerade.Lazy;
using OneOfZero.DotMasquerade.Tests.Utilities;
using OneOfZero.DotMasquerade.Tests.Lazy.Structures;

namespace OneOfZero.DotMasquerade.Tests.Lazy
{
	[TestFixture]
	public class GenericTests : ActivityTester
	{
		private readonly Activity _callbackInvocation = new Activity("Callback invoked");

		private GenericStructure<string> CreateSimpleProxy()
		{
			return LazyProxy.Create(() =>
			{
				this.LogActivity(this._callbackInvocation);
				return new GenericStructure<string>();
			});
		}

		[Test]
		public void TestPropertyGet()
		{
			GenericStructure<string> obj;
			string tmp;

			obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
			tmp = obj.GenericProperty;
			this.AssertLogged(this._callbackInvocation);
		}

		[Test]
		public void TestPropertySet()
		{
			GenericStructure<string> obj;

			obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
			obj.GenericProperty = "asdf";
			this.AssertLogged(this._callbackInvocation);
			Assert.AreEqual(obj.GenericProperty, "asdf", "Value stored in property was not persisted");
		}

		[Test]
		public void TestMethodInvoke()
		{
			GenericStructure<string> obj;
			string result;

			obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
			result = obj.GenericMethod("asdf");
			this.AssertLogged(this._callbackInvocation);
			Assert.AreEqual(result, "asdf");
		}
	}
}
