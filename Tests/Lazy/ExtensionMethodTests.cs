﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using OneOfZero.DotMasquerade.Lazy;
using OneOfZero.DotMasquerade.Tests.Utilities;
using OneOfZero.DotMasquerade.Tests.Lazy.Structures;

#if !__MonoCS__
namespace OneOfZero.DotMasquerade.Tests.Lazy
{
	[TestFixture]
	public class ExtensioMethodTests : ActivityTester
	{
		private readonly Activity _callbackInvocation = new Activity("Callback invoked");

		private BasicStructure CreateSimpleProxy()
		{
			return LazyProxy.Create(() =>
			{
				this.LogActivity(this._callbackInvocation);
				return new BasicStructure();
			});
		}

		[Test]
		public void TestIsProxyExt()
		{
			BasicStructure obj;

			obj = this.CreateSimpleProxy();
			Assert.True(obj.IsProxy());

			obj = new BasicStructure();
			Assert.False(obj.IsProxy());
		}

		[Test]
		public void TestLoadExt()
		{
			BasicStructure obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
			obj.LazyLoad();
			this.AssertLogged(this._callbackInvocation);
		}

		[Test]
		public void TestUnpackExt()
		{
			BasicStructure obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
			Assert.True(obj.IsProxy());
			obj = obj.Unpack();
			this.AssertLogged(this._callbackInvocation);
			Assert.False(obj.IsProxy());
		}
	}
}
#endif