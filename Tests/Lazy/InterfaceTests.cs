﻿using System;
using NUnit.Framework;
using OneOfZero.DotMasquerade.Tests.Utilities;
using OneOfZero.DotMasquerade.Tests.Lazy.Structures;
using OneOfZero.DotMasquerade.Lazy;

namespace OneOfZero.DotMasquerade.Tests.Lazy
{
	[TestFixture]
	public class InterfaceTests : ActivityTester
	{
		private readonly Activity _callbackInvocation = new Activity("Callback invoked");

		private IBasicStructure CreateSimpleProxy()
		{
			return LazyProxy.Create(() =>
			{
				this.LogActivity(this._callbackInvocation);
				return new BasicStructure();
			});
		}

		[Test]
		public void TestNoAccessInterface()
		{
			IBasicStructure obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
		}

		[Test]
		public void TestIsProxyInterface()
		{
			IBasicStructure obj;

			obj = this.CreateSimpleProxy();
			Assert.True(LazyProxy.IsProxy(obj));

			obj = new BasicStructure();
			Assert.False(LazyProxy.IsProxy(obj));
		}

		[Test]
		public void TestLoadInterface()
		{
			IBasicStructure obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);

			LazyProxy.Load(obj);

			this.AssertLogged(this._callbackInvocation);
		}

		[Test]
		public void TestUnpackInterface()
		{
			IBasicStructure obj = this.CreateSimpleProxy();

			this.AssertNotLogged(this._callbackInvocation);
			Assert.True(LazyProxy.IsProxy(obj), "The object is not a proxy (while it should be)");

			obj = LazyProxy.Unpack(obj);

			this.AssertLogged(this._callbackInvocation);
			Assert.False(LazyProxy.IsProxy(obj), "The object is a proxy (while it shouldn't be)");
		}

		[Test]
		public void TestPropertyGetInterface()
		{
			IBasicStructure obj;
			string tmp;

			obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
			tmp = obj.SimpleProperty;
			this.AssertLogged(this._callbackInvocation);

			this.ResetLog();

			obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
			tmp = obj.PrivateSetProperty;
			this.AssertLogged(this._callbackInvocation);

			this.ResetLog();

			obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
			tmp = obj.PrivateSetProperty;
			this.AssertLogged(this._callbackInvocation);
		}

		[Test]
		public void TestPropertySetInterface()
		{
			IBasicStructure obj;

			obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
			obj.SimpleProperty = "asdf";
			this.AssertLogged(this._callbackInvocation);
			Assert.AreEqual(obj.SimpleProperty, "asdf", "Value stored in property was not persisted");

			this.ResetLog();

			obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
			obj.PrivateGetProperty = "asdf";
			this.AssertLogged(this._callbackInvocation);
		}

		[Test]
		public void TestMethodInvokeInterface()
		{
			IBasicStructure obj, tmp;

			obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
			obj.SimpleMethod();
			this.AssertLogged(this._callbackInvocation);
			Assert.True(obj.SimpleMethodCalled);

			this.ResetLog();

			obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
			obj.ComplexParameterMethod("a", new string[] { "b", "c" });
			this.AssertLogged(this._callbackInvocation);
			Assert.AreEqual(obj.ComplexParmaterMethodParameterA, "a");
			Assert.AreEqual(obj.ComplexParmaterMethodParameterB, new string[] { "b", "c" });

			this.ResetLog();

			obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
			tmp = obj.GenericReturnTypeMethod<BasicStructure>();
			this.AssertLogged(this._callbackInvocation);
			Assert.NotNull(tmp);
			Assert.True(typeof(BasicStructure) == tmp.GetType());

			this.ResetLog();

			obj = this.CreateSimpleProxy();
			this.AssertNotLogged(this._callbackInvocation);
			tmp = new BasicStructure();
			obj.GenericParameterTypeMethod(tmp);
			this.AssertLogged(this._callbackInvocation);
			object.ReferenceEquals(obj.GenericParameterTypeMethodParameter, tmp);
		}
	}
}
