﻿using System;

namespace OneOfZero.DotMasquerade.Tests.Lazy.Structures
{
	public class BasicStructure : IBasicStructure
	{
		public virtual string SimpleProperty { get; set; }

		public virtual string PrivateGetProperty { private get; set; }

		public virtual string PrivateSetProperty { get; private set; }

		public virtual bool SimpleMethodCalled { get; private set; }

		public virtual string ComplexParmaterMethodParameterA { get; private set; }

		public virtual string[] ComplexParmaterMethodParameterB { get; private set; }

		public virtual object GenericParameterTypeMethodParameter { get; private set; }

		public virtual void SimpleMethod()
		{
			this.SimpleMethodCalled = true;
		}

		public virtual void ComplexParameterMethod(string parameterA, string[] parameterB)
		{
			this.ComplexParmaterMethodParameterA = parameterA;
			this.ComplexParmaterMethodParameterB = parameterB;
		}

		public virtual T GenericReturnTypeMethod<T>()
		{
			return (T)Activator.CreateInstance(typeof(T));
		}

		public virtual void GenericParameterTypeMethod<T>(T parameter)
		{
			this.GenericParameterTypeMethodParameter = parameter;
		}
	}
}
