﻿using System;

namespace OneOfZero.DotMasquerade.Tests.Lazy.Structures
{
	public class GenericStructure<T>
	{
		public virtual T GenericProperty { get; set; }

		public virtual T GenericMethod(T input)
		{
			return input;
		}
	}
}
