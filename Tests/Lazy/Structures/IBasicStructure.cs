﻿using System;

namespace OneOfZero.DotMasquerade.Tests.Lazy.Structures
{
	public interface IBasicStructure
	{
		string SimpleProperty { get; set; }

		string PrivateGetProperty { set; }

		string PrivateSetProperty { get; }

		bool SimpleMethodCalled { get; }

		string ComplexParmaterMethodParameterA { get; }

		string[] ComplexParmaterMethodParameterB { get; }

		object GenericParameterTypeMethodParameter { get; }

		void SimpleMethod();

		void ComplexParameterMethod(string parameterA, string[] parameterB);

		T GenericReturnTypeMethod<T>();

		void GenericParameterTypeMethod<T>(T parameter);
	}
}
