﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using Mono.Cecil;

namespace OneOfZero.DotMasquerade.Invasive
{
	internal static class Utilities
	{
		public static ICollection<T> FindAssemblyAttributes<T>(this Module module)
			where T : Attribute
		{
			return module.Assembly.GetCustomAttributes(typeof(T), false)
				.Cast<T>()
				.ToList()
				;
		}

		public static ICollection<Type> FindTypesWithAttribute<T>(
			this Module module) where T : Attribute
		{
			return module.FindTypes(
				(f, c) => f.GetCustomAttributes(typeof(T), false).Any(),
				null
				);
		}

		public static string GetFilePath(this Assembly assembly)
		{
			return new Uri(assembly.CodeBase).LocalPath;
		}

		public static AssemblyDefinition GetCecil(this Assembly assembly)
		{
			return AssemblyDefinition.ReadAssembly(assembly.GetFilePath());
		}

		public static ModuleDefinition GetModule(this AssemblyDefinition assembly, Module module)
		{
			return assembly.Modules.FirstOrDefault(m => m.Name == module.Name);
		}

		public static TypeDefinition GetType(this ModuleDefinition module, Type type)
		{
			return module.GetType(type.FullName);
		}

		public static bool IsFromProtectedAssembly(this Type type)
		{
			object[] attributes = type.Assembly.GetCustomAttributes(
				typeof(AssemblyCompanyAttribute), false
			);

			return attributes.OfType<AssemblyCompanyAttribute>().Any(
				attr => attr.Company == "Microsoft Corporation" || attr.Company == "1of0.net"
			);
		}
	}
}