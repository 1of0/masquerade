﻿using System;
using System.Reflection;
using Mono.Cecil;

namespace OneOfZero.DotMasquerade.Invasive
{
	public class ModulePackage
	{
		public Module Reflection { get; set; }

		public ModuleDefinition Cecil { get; set; }
	}
}
