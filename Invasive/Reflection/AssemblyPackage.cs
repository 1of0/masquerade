﻿using System;
using System.Reflection;
using Mono.Cecil;

namespace OneOfZero.DotMasquerade.Invasive
{
	public class AssemblyPackage
	{
		public Assembly Reflection { get; set; }

		public AssemblyDefinition Cecil { get; set; }
	}
}
