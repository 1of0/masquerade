﻿using System;

namespace OneOfZero.DotMasquerade.Invasive
{
	/// <summary>
	/// <para>
	/// Indicates that the configured class should support lazy initialization. To achieve this, an
	/// interface will be created of the type. The generated interface will be implemented by the 
	/// lazy type and provided type.
	/// </para>
	/// <para>
	/// While it is possible to use this on external assemblies, note that this will modify the 
	/// external assembly by referencing the dynamic binary, and by adding the generated interface
	/// to the list of interfaces for the type, on each compilation. This might be an issue when the
	/// external assembly is shared or not writable. Also make sure that 
	/// modifications do not violate the external assembly's license.
	/// </para>
	/// </summary>
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
	public class SupportLazyExternalAttribute : Attribute
	{
		/// <summary>
		/// Holds a type outside the primary assembly that should be prepared for lazy 
		/// initialization.
		/// </summary>
		public Type ExternalType { get; private set; }

		/// <summary>
		/// If an external type is specified, this property is to explicitly make sure that the
		/// developer is aware of the consequences of this attribute on external assemblies.
		/// (at least we tried)
		/// </summary>
		public bool AllowExternalAssemblyModification { get; set; }

		/// <summary>
		/// <seealso cref="SupportLazyExternalAttribute.AllowExternalAssemblyModification"/> must be
		/// enabled for this to work, so that the implications of this are expressed.
		/// The interface will be generated for the specified <paramref name="externalType"/>, and 
		/// the specified type will implement the generated interface. 
		/// </summary>
		/// <param name="externalType">Type that should support lazy initialization</param>
		public SupportLazyExternalAttribute(Type externalType)
		{
			this.ExternalType = externalType;
		}
	}
}
