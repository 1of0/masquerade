﻿using System;

namespace OneOfZero.DotMasquerade.Invasive
{
	/// <summary>
	/// <para>
	/// Indicates that the tagged class should support lazy initialization. To achieve this, an 
	/// interface will be created of the type. The generated interface will be implemented by the 
	/// lazy type and provided type.
	/// </para>
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public class SupportLazyAttribute : Attribute
	{
	}
}
