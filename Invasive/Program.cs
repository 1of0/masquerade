﻿using System;
using System.Reflection;
using OneOfZero.DotMasquerade.Invasive.LazyProxy;

namespace OneOfZero.DotMasquerade.Invasive
{
	static class Program
	{
		static void Main(string[] args)
		{
			if (args.Length == 0)
			{
				return;
			}

			Assembly assembly = Assembly.LoadFile(args[0]);

			AssemblyPackage assemblyPackage = new AssemblyPackage()
			{
				Reflection = assembly,
				Cecil = assembly.GetCecil()
			};

			foreach (Module module in assembly.GetModules(false))
			{
				ModulePackage modulePackage = new ModulePackage()
				{
					Reflection = module,
					Cecil = assemblyPackage.Cecil.GetModule(module)
				};

				new LazyProxyScanner(modulePackage).Run();
			}

			assemblyPackage.Cecil.Write(assemblyPackage.Reflection.GetFilePath());

		}
	}
}
