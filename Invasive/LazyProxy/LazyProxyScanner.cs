﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Mono.Cecil;

namespace OneOfZero.DotMasquerade.Invasive.LazyProxy
{
	public class LazyProxyScanner
	{
		private ModulePackage module;

		public LazyProxyScanner(ModulePackage module)
		{
			this.module = module;
		}

		public void Run()
		{
			var types = this.ScanTypes();

			foreach (Type type in types)
			{

			}
		}

		private IEnumerable<Type> ScanTypes()
		{
			var types = this.module.Reflection
				.FindTypesWithAttribute<SupportLazyAttribute>();

			var externalTypeAttributes = this.module.Reflection
				.FindAssemblyAttributes<SupportLazyExternalAttribute>();

			foreach (var externalTypeAttribute in externalTypeAttributes)
			{
				if (externalTypeAttribute.AllowExternalAssemblyModification &&
					externalTypeAttribute.ExternalType != null &&
					!externalTypeAttribute.ExternalType.IsFromProtectedAssembly())
				{
					types.Add(externalTypeAttribute.ExternalType);
				}
			}
		}
	}
}
