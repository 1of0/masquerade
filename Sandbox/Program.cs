﻿using System;
using System.Reflection;

//using OneOfZero.DotMasquerade.Lazy;
using OneOfZero.DotMasquerade.Invasive;

[assembly: SupportLazyExternal(typeof(System.Version), AllowExternalAssemblyModification = true)]

namespace OneOfZero.DotMasquerade.Sandbox
{
	class Program
	{
		static void Main(string[] args)
		{
			/*
			Console.WriteLine("========");
			Console.WriteLine(" Case A ");
			Console.WriteLine("========");

			Subject a = LazyProxy.Create(() =>
			{
				Console.WriteLine("Late loaded an instance for Subject 'a'");
				return new Subject();
			});
			Console.WriteLine("Wrapped 'a' in lazy proxy");
			Console.WriteLine("Calling a.Lorem");
			Console.WriteLine("a.Lorem returned: {0}", a.Lorem());

			Console.WriteLine();
			Console.WriteLine("========");
			Console.WriteLine(" Case B ");
			Console.WriteLine("========");

			Subject b = LazyProxy.Create(() =>
			{
				Console.WriteLine("Late loaded an instance for Subject 'b'");
				return new Subject();
			});
			Console.WriteLine("Wrapped 'b' in lazy proxy");
			Console.WriteLine("Setting value of b.Baz");
			b.Baz = "asdf";
			Console.WriteLine("Value of b.Baz was set");
			Console.WriteLine("Value of b.Baz: {0}", b.Baz);
			*/


			Console.Read();
		}
	}


	public interface ISubject
	{
		string Foo { get; set; }

		string Bar { get; }

		string Baz { set; }

		string Lorem();
	}

	public class Subject : ISubject
	{
		public string Foo { get; set; }

		public string Bar { get; private set; }

		public string Baz { private get; set; }

		public string Lorem()
		{
			return "ipsum";
		}
	}
}
