﻿using System;
using System.Linq;
using System.Threading;
using System.Reflection;
using System.Reflection.Emit;

namespace OneOfZero.DotMasquerade.Utilities
{
	/// <summary>
	/// Holds a bunch of System.Reflection.Emit extensions and utilities
	/// </summary>
	public static class ILHelper
	{
		/// <summary>
		/// Extension method for DefineConstructor that allows <paramref name="implementation"/> as
		/// an argument
		/// </summary>
		/// <param name="typeBuilder">TypeBuilder to use</param>
		/// <param name="attributes">Attributes for the constructor method</param>
		/// <param name="callingConvention">Calling convention for the constructor method</param>
		/// <param name="parameterTypes">Parametertypes for the constructor method</param>
		/// <param name="implementation">Action that implements the CIL for the constructor method</param>
		/// <returns>Resulting ConstructorBuilder instance</returns>
		public static ConstructorBuilder DefineConstructor(this TypeBuilder typeBuilder,
			MethodAttributes attributes, CallingConventions callingConvention,
			Type[] parameterTypes, Action<ILGenerator> implementation)
		{
			ConstructorBuilder builder = typeBuilder.DefineConstructor(attributes, callingConvention, parameterTypes);

			if (implementation != null)
				implementation(builder.GetILGenerator());

			return builder;
		}

		/// <summary>
		/// Extension method for DefineMethod that allows <paramref name="implementation"/> as an 
		/// argument
		/// </summary>
		/// <param name="typeBuilder">TypeBuilder to use</param>
		/// <param name="name">Name of the method</param>
		/// <param name="attributes">Attributes for the method</param>
		/// <param name="returnType">Return type for the method</param>
		/// <param name="parameterTypes">Parametertypes for the method</param>
		/// <param name="implementation">Action that implements the CIL for the method</param>
		/// <returns>Resulting MethodBuilder instance</returns>
		public static MethodBuilder DefineMethod(this TypeBuilder typeBuilder, string name,
			MethodAttributes attributes, Type returnType, Type[] parameterTypes,
			Action<ILGenerator> implementation)
		{
			MethodBuilder builder = typeBuilder.DefineMethod(
				name,
				attributes,
				returnType,
				parameterTypes
			);

			if (implementation != null)
				implementation(builder.GetILGenerator());

			return builder;
		}

		/// <summary>
		/// Loads the value of the provided <paramref name="field"/> onto the stack
		/// </summary>
		/// <param name="il">ILGenerator</param>
		/// <param name="field">Field to load the value of</param>
		public static void LoadFieldValue(this ILGenerator il, FieldBuilder field)
		{
			il.Emit(OpCodes.Ldarg_0);
			il.Emit(OpCodes.Ldfld, field);
		}

		/// <summary>
		/// Puts the address of the provided <paramref name="field"/> onto the stack
		/// </summary>
		/// <param name="il">ILGenerator</param>
		/// <param name="field">Field to get the address of</param>
		public static void LoadFieldAddress(this ILGenerator il, FieldBuilder field)
		{
			il.Emit(OpCodes.Ldarg_0);
			il.Emit(OpCodes.Ldflda, field);
		}

		/// <summary>
		/// Saves the value that is on top of the stack to the provided <paramref name="field"/>
		/// </summary>
		/// <param name="il">ILGenerator</param>
		/// <param name="field">Field to save to</param>
		public static void SaveValueToField(this ILGenerator il, FieldBuilder field)
		{
			il.SaveValueToField(field, null);
		}

		/// <summary>
		/// Saves the value that is on top of the stack after executing the 
		/// <paramref name="process"/> action to the provided field
		/// </summary>
		/// <param name="il">ILGenerator</param>
		/// <param name="field">Field to save to</param>
		/// <param name="process">Process that should leave the value that is to be saved on top of
		/// the stack</param>
		public static void SaveValueToField(this ILGenerator il, FieldBuilder field, Action process)
		{
			if (process != null)
			{
				// Load the instance reference before having the process place the value that is to
				// be stored on top
				il.Emit(OpCodes.Ldarg_0);
				process();

				// Stfld(this, value)
				il.Emit(OpCodes.Stfld, field);
			}
			else
			{
				// Store top of the stack in tmp
				LocalBuilder tmp = il.DeclareLocal(field.FieldType);
				il.Emit(OpCodes.Stloc, tmp);

				// Put the instance reference ('this') on stack before placing the value of tmp on
				// top again
				il.Emit(OpCodes.Ldarg_0);
				il.Emit(OpCodes.Ldloca_S, tmp);

				// Stfld(this, value)
				il.Emit(OpCodes.Stfld, field);
			}
		}

		/// <summary>
		/// Calls the specified constructor on the "current" instance context
		/// </summary>
		/// <param name="il">ILGenerator</param>
		/// <param name="constructor">Constructor to call</param>
		public static void CallMethod(this ILGenerator il, ConstructorInfo constructor)
		{
			il.Emit(OpCodes.Ldarg_0);
			il.Emit(OpCodes.Call, constructor);
		}

		/// <summary>
		/// Calls the specified method on the "current" instance context
		/// </summary>
		/// <param name="il">ILGenerator</param>
		/// <param name="method">Method to call</param>
		public static void CallMethod(this ILGenerator il, MethodInfo method)
		{
			il.Emit(OpCodes.Ldarg_0);
			il.Emit(OpCodes.Call, method);
		}

		/// <summary>
		/// Calls the specified method on the provided <paramref name="instanceReference"/> context
		/// </summary>
		/// <param name="il">ILGenerator</param>
		/// <param name="instanceReference">Instance reference for context</param>
		/// <param name="method">Method to call</param>
		public static void CallMethod(this ILGenerator il, FieldBuilder instanceReference,
			MethodInfo method)
		{
			il.LoadFieldValue(instanceReference);
			il.Emit(OpCodes.Call, method);
		}


		/// <summary>
		/// Calls the specified method on the "current" instance context. Arguments can be loaded by
		/// providing an implementation for <paramref name="argumentLoading"/>
		/// </summary>
		/// <param name="il">ILGenerator</param>
		/// <param name="method">Method to call</param>
		/// <param name="argumentLoading">Process that should load the method arguments onto the
		/// stack</param>
		public static void CallMethod(this ILGenerator il, MethodInfo method,
			Action argumentLoading)
		{
			il.Emit(OpCodes.Ldarg_0);

			if (argumentLoading != null)
				argumentLoading();

			il.Emit(OpCodes.Call, method);
		}

		/// <summary>
		/// Calls the specified method on the provided <paramref name="instanceReference"/> context.
		/// Arguments can be loaded by providing an implementation for 
		/// <paramref name="argumentLoading"/>
		/// </summary>
		/// <param name="il">ILGenerator</param>
		/// <param name="instanceReference">Instance reference for context</param>
		/// <param name="method">Method to call</param>
		/// <param name="argumentLoading">Process that should load the method arguments onto the
		/// stack</param>
		public static void CallMethod(this ILGenerator il, FieldBuilder instanceReference,
			MethodInfo method, Action argumentLoading)
		{
			il.LoadFieldValue(instanceReference);

			if (argumentLoading != null)
				argumentLoading();

			il.Emit(OpCodes.Call, method);
		}


		/// <summary>
		/// Begins a block of that requires mutual exclusion of processing
		/// </summary>
		/// <param name="il">ILGenerator</param>
		/// <param name="lockField">Field that holds the synchronization object</param>
		/// <param name="lockReference">Local field that holds a reference to the synchronization
		/// object</param>
		public static void LockBegin(this ILGenerator il, FieldBuilder lockField,
			out LocalBuilder lockReference)
		{
			lockReference = il.DeclareLocal(typeof(object));

			il.BeginExceptionBlock();
			il.LoadFieldValue(lockField);
			il.Emit(OpCodes.Dup);
			il.Emit(OpCodes.Stloc, lockReference); // lockReference
			il.Emit(OpCodes.Call, ILHelper.References.MonitorEnter);
		}

		/// <summary>
		/// Releases a lock
		/// </summary>
		/// <param name="il">ILGenerator</param>
		/// <param name="lockReference">Local field that holds a reference to the synchronization
		/// object</param>
		public static void LockRelease(this ILGenerator il, LocalBuilder lockReference)
		{
			Label endFinally = il.DefineLabel();

			il.BeginFinallyBlock();

			il.Emit(OpCodes.Ldloc, lockReference); // lockReference
			il.Emit(OpCodes.Call, ILHelper.References.MonitorExit);

			il.EndExceptionBlock();
		}

		/// <summary>
		/// Returns whether or not the <paramref name="value"/> has the provided 
		/// <paramref name="flags"/>
		/// </summary>
		/// <param name="value">Value to test against</param>
		/// <param name="flags">Flags to test with</param>
		/// <returns>Whether or not the <paramref name="value"/> has the provided 
		/// <paramref name="flags"/></returns>
		public static bool HasFlag(this Enum value, Enum flags)
		{
			ulong valueVal = Convert.ToUInt64(value);
			ulong flagsVal = Convert.ToUInt64(flags);

			return (valueVal & flagsVal) == flagsVal;
		}

		/// <summary>
		/// Nested class that houses various common attribute sets and method references
		/// </summary>
		public static class References
		{
			/// <summary>
			/// Holds a reference to the root object constructor method
			/// </summary>
			public static readonly ConstructorInfo RootObjectConstructor =
				typeof(Object).GetConstructor(Type.EmptyTypes);

			/// <summary>
			/// Holds a reference to the Enter method of the System.Threading.Monitor class
			/// </summary>
			// I know this looks ugly, but typeof(ref bool) and typeof(bool&) don't work
			public static readonly MethodInfo MonitorEnter =
				typeof(Monitor).GetMethod("Enter", new Type[] { typeof(object) });

			/// <summary>
			/// Holds a reference to the Exit method of the System.Threading.Monitor class
			/// </summary>
			public static readonly MethodInfo MonitorExit =
				typeof(Monitor).GetMethod("Exit", new Type[] { typeof(object) });
		}
	}
}
