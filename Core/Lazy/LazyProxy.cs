﻿using System;
using System.Linq;
using System.Threading;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;

namespace OneOfZero.DotMasquerade.Lazy
{
	/// <summary>
	/// Provides creation and helper methods for lazy proxies
	/// </summary>
	public class LazyProxy
	{
		static LazyProxy()
		{
			// In a non-debug situation we might not have write privileges to the application path,
			// so flushing to disk might not be an option. Besides, the flushing does not act as a
			// caching mechanism, and is merely a debug tool.
#if DEBUG
			FlushProxiesToDisk = true;
#else
			FlushProxiesToDisk = false;
#endif
			// Register a hook for the quitting of the application, so we can flush the generated
			// proxies to disk.
			AppDomain.CurrentDomain.ProcessExit += ProcessExit;
		}

		/// <summary>
		/// Class lock object
		/// </summary>
		private static object _syncRoot = new object();

		/// <summary>
		/// Holds an instance of the assembly builder
		/// </summary>
		protected static AssemblyBuilder _assemblyBuilder = Thread.GetDomain()
			.DefineDynamicAssembly(
				new AssemblyName("OneOfZero.DotMasquerade.Lazy.ProxyCache"),
				AssemblyBuilderAccess.RunAndSave
			)
		;

		/// <summary>
		/// Holds an instance of the module builder
		/// </summary>
		protected static ModuleBuilder _moduleBuilder = _assemblyBuilder.DefineDynamicModule(
			_assemblyBuilder.GetName().Name,
			_assemblyBuilder.GetName().Name + ".dll"
		);

		/// <summary>
		/// Holds a collection of proxies that have already been created.
		/// </summary>
		protected static Dictionary<Type, Type> _cachedProxyTypes = new Dictionary<Type, Type>();

		/// <summary>
		/// Gets or sets a boolean value that indicates whether or not cached proxies should be
		/// flushed to disk. The filename will be OneOfZero.DotMasquerade.Lazy.ProxyCache.dll.
		/// </summary>
		public static bool FlushProxiesToDisk { get; set; }

		/// <summary>
		/// Creates a proxy of type <typeparamref name="T"/> and returns an instance of the proxy
		/// (as <typeparamref name="T"/>) with the provided <paramref name="lateCallback"/>.
		/// Generated proxies are automatically cached per type and the lateCallback is not fixed
		/// to a generated proxy. This means that calling this method with various callbacks for the
		/// same type of <typeparamref name="T"/>, is not expensive (in terms of code generation).
		/// </summary>
		/// <typeparam name="T">Type to wrap proxy around</typeparam>
		/// <param name="lateCallback">Callback that is invoked when a public virtual property or 
		/// public virtual method of type <typeparamref name="T"/> is accessed</param>
		/// <returns>A proxy of type <typeparamref name="T"/> as <typeparamref name="T"/></returns>
		public static T Create<T>(Func<T> lateCallback)
		{
			if (!LazyProxy._cachedProxyTypes.ContainsKey(typeof(T)))
			{
				lock (LazyProxy._syncRoot)
				{
					Type newProxy = new LazyProxyBuilder<T>(LazyProxy._moduleBuilder).CreateType();
					LazyProxy._cachedProxyTypes.Add(typeof(T), newProxy);
					return (T)Activator.CreateInstance(newProxy, lateCallback);
				}
			}

			Type proxyType = LazyProxy._cachedProxyTypes[typeof(T)];
			return (T)Activator.CreateInstance(proxyType, lateCallback);
		}

		/// <summary>
		/// If the provided <paramref name="instance"/> is a proxy, this method makes sure that the
		/// underlying object is loaded using the late callback.
		/// </summary>
		/// <typeparam name="T">Proxied type</typeparam>
		/// <param name="instance">Proxy instance</param>
		public static void Load<T>(T instance)
		{
			if (LazyProxy.IsProxy(instance))
				(instance as ILazyProxy).Load();
		}

		/// <summary>
		/// If the provided <paramref name="instance"/> is a proxy, this method returns the 
		/// underlying object, so that the proxy may be disposed. If the provided 
		/// <paramref name="instance"/> isn't a proxy, the <paramref name="instance"/> will be 
		/// returned as is.
		/// </summary>
		/// <typeparam name="T">Proxied type</typeparam>
		/// <param name="instance">Proxy instance</param>
		/// <returns>Unproxied instance</returns>
		public static T Unpack<T>(T instance)
		{
			if (LazyProxy.IsProxy(instance))
				return (instance as ILazyProxy).Unpack<T>();

			return instance;
		}

		/// <summary>
		/// Returns a boolean value indicating whether or not the provided 
		/// <paramref name="instance"/> is a proxy.
		/// </summary>
		/// <typeparam name="T">Proxied type</typeparam>
		/// <param name="instance">Proxy instance?</param>
		/// <returns>Wether or not the provided instance is a proxy</returns>
		public static bool IsProxy<T>(T instance)
		{
			return instance.GetType().GetInterfaces()
				.Count(t => t == typeof(ILazyProxy)) > 0
			;
		}

		/// <summary>
		/// Application exit hook
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected static void ProcessExit(object sender, EventArgs e)
		{
			if (LazyProxy.FlushProxiesToDisk)
			{
				string filename = _assemblyBuilder.GetName().Name + ".dll";
				File.Delete(filename);
				_assemblyBuilder.Save(filename);
			}
		}
	}
}
