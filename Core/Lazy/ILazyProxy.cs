﻿using System;

namespace OneOfZero.DotMasquerade.Lazy
{
	/// <summary>
	/// Interface that is implemented on all proxies made by the LazyProxyBuilder
	/// </summary>
	public interface ILazyProxy
	{
		/// <summary>
		/// Forces the late callback to initialize the proxied instance
		/// </summary>
		void Load();

		/// <summary>
		/// Returns the loaded and unproxied instance (may be desirable for performance)
		/// </summary>
		/// <typeparam name="T">Proxied type</typeparam>
		/// <returns>Unpacked instance</returns>
		T Unpack<T>();
	}
}
