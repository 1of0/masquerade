﻿using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using OneOfZero.DotMasquerade.Utilities;

/**
 *	TODO:
 *		- It would be nice to be able to subscribe to events before the proxied instance is loaded.
 *		  This means that a virtual event should be generated, of which subscribers should be
 *		  transferred to the proxied instance when it's loaded.
 *		- Perhaps throw an exception or warning when a method or property is public, but not 
 *		  virtual. Because when that's the case, method or property invocation can not be detected,
 *		  and no forwards can be done to the internal methods.
 */

namespace OneOfZero.DotMasquerade.Lazy
{
	/// <summary>
	/// Generates a runtime created lazy proxy over type <typeparamref name="T"/> that extends 
	/// <typeparamref name="T"/> and implements <c>ILazyProxy</c>
	/// </summary>
	/// <typeparam name="T">Type that is to be proxied</typeparam>
	internal class LazyProxyBuilder<T>
	{
		/// <summary>
		/// Holds a reference to the type builder
		/// </summary>
		protected TypeBuilder _typeBuilder;

		/// <summary>
		/// Holds a reference to the "__syncRoot" field
		/// </summary>
		protected FieldBuilder _syncRootField;

		/// <summary>
		/// Holds a reference to the "__lateCallback" field
		/// </summary>
		protected FieldBuilder _callbackField;

		/// <summary>
		/// Holds a reference to the "__loadedInstance" field
		/// </summary>
		protected FieldBuilder _loadedInstanceField;

		/// <summary>
		/// Holds a reference to the proxy constructor
		/// </summary>
		protected ConstructorBuilder _constructor;

		/// <summary>
		/// Holds a reference to the generated "Load" method
		/// </summary>
		protected MethodBuilder _loadMethod;

		/// <summary>
		/// Holds a reference to the generated "Unpack" method
		/// </summary>
		protected MethodBuilder _unpackMethod;

		/// <summary>
		/// Holds a list of known getters and setters
		/// </summary>
		protected List<MethodInfo> _knownGettersAndSetters;

		/// <summary>
		/// Initializes an instance of the LazyProxyBuilder class to create a proxy for the type
		/// <typeparamref name="T"/>
		/// </summary>
		/// <param name="moduleBuilder">Module builder to work under</param>
		public LazyProxyBuilder(ModuleBuilder moduleBuilder)
		{
			this._knownGettersAndSetters = new List<MethodInfo>();

			// The lazy proxy will extend T, unless T is an interface
			Type baseType = typeof(T).IsInterface ? null : typeof(T);

			// The lazy proxy will implement ILazyProxy, and T if T is an interface
			Type[] interfaces = typeof(T).IsInterface
				? new Type[] { typeof(ILazyProxy), typeof(T) }
				: new Type[] { typeof(ILazyProxy) }
			;

			this._typeBuilder = moduleBuilder.DefineType(
				this.GetLazyName(typeof(T)),
				TypeAttributes.Public,
				baseType,
				interfaces
			);
		}

		/// <summary>
		/// Returns the name of the proxy type
		/// </summary>
		/// <param name="type">Type to proxy</param>
		/// <returns>Name for the proxied type</returns>
		protected string GetLazyName(Type type)
		{
			string typeName = type.FullName.Replace(".", "_");
			return typeName + "__Lazy";
		}

		/// <summary>
		/// Creates and returns a proxy for type <typeparamref name="T"/>
		/// </summary>
		/// <returns>Newly created proxy type for <typeparamref name="T"/></returns>
		public Type CreateType()
		{
			this.GenerateFields()
				.GenerateConstructor()
				.GenerateLoadMethod()
				.GenerateUnpackMethod()
				.GeneratePropertyProxies()
				.GenerateMethodProxies();

			return this._typeBuilder.CreateType();
		}

		/// <summary>
		/// Generates the required fields on the TypeBuilder
		/// </summary>
		/// <returns>Self for chaining</returns>
		public LazyProxyBuilder<T> GenerateFields()
		{
			// Holds the lock object
			this._syncRootField = this._typeBuilder.DefineField(
				"__syncRoot", typeof(object), FieldAttributes.Private);

			// Holds the callback
			this._callbackField = this._typeBuilder.DefineField(
				"__lateCallback", typeof(Func<T>), FieldAttributes.Private);

			// Holds the lazy loaded instance when the instance is initialized
			this._loadedInstanceField = this._typeBuilder.DefineField(
				"__loadedInstance", typeof(T), FieldAttributes.Private);

			return this;
		}

		/// <summary>
		/// Generates the type constructor
		/// </summary>
		/// <returns>Self for chaining</returns>
		public LazyProxyBuilder<T> GenerateConstructor()
		{
			this._constructor = _typeBuilder.DefineConstructor(
				MethodAttributes.Public,
				CallingConventions.Standard,
				new Type[] { typeof(Func<T>) },
				il =>
				{
					// Initialize a lock object for the instance
					il.Emit(OpCodes.Ldarg_0);
					il.Emit(OpCodes.Newobj, ILHelper.References.RootObjectConstructor);
					il.Emit(OpCodes.Stfld, this._syncRootField);
					// Call parent type constructor
					il.CallMethod(ILHelper.References.RootObjectConstructor);
					// Store callback from arg_1 in callbackField 
					il.SaveValueToField(this._callbackField, () => il.Emit(OpCodes.Ldarg_1));
					il.Emit(OpCodes.Ret);
				}
			);
			return this;
		}

		/// <summary>
		/// Generates the "Load" method <seealso cref="ILazyProxy.Load"/>
		/// </summary>
		/// <returns>Self for chaining</returns>
		protected LazyProxyBuilder<T> GenerateLoadMethod()
		{
			this._loadMethod = _typeBuilder.DefineMethod(
				"Load",
				MethodAttributes.Public | MethodAttributes.Virtual,
				typeof(void),
				Type.EmptyTypes,
				il =>
				{
					Label skipLazyLoad = il.DefineLabel();
					LocalBuilder lockReference;
					LocalBuilder loadedInstance = il.DeclareLocal(typeof(T));

					// Begin mutex block
					il.LockBegin(this._syncRootField, out lockReference);

					// Compare cachedInstanceField and null
					il.LoadFieldValue(this._loadedInstanceField);
					il.Emit(OpCodes.Brtrue_S, skipLazyLoad);

					// if cachedInstanceField is null
					il.SaveValueToField(this._loadedInstanceField, () =>
					{
						// Load delegate
						il.LoadFieldValue(this._callbackField);
						// Execute the invoke method on the loaded delegate
						il.Emit(OpCodes.Callvirt, typeof(Func<T>).GetMethod("Invoke"));
					});
					// endif cachedInstanceField is null
					il.MarkLabel(skipLazyLoad);

					// Release mutex
					il.LockRelease(lockReference);
					il.Emit(OpCodes.Ret);
				}
			);

			return this;
		}

		/// <summary>
		/// Generates the "Unpack" method <seealso cref="ILazyProxy.Unpack<T>"/>
		/// </summary>
		/// <returns>Self for chaining</returns>
		protected LazyProxyBuilder<T> GenerateUnpackMethod()
		{
			this._unpackMethod = _typeBuilder.DefineMethod(
				"Unpack",
				MethodAttributes.Public | MethodAttributes.Virtual,
				null,
				Type.EmptyTypes,
				il =>
				{
					// Ensure that the proxied instance is initialized
					il.CallMethod(this._loadMethod);

					// Load the proxied instance for returning
					il.LoadFieldValue(this._loadedInstanceField);

					il.Emit(OpCodes.Ret);
				}
			);

			// Declare generic <T>
			GenericTypeParameterBuilder[] typeParameters =
				this._unpackMethod.DefineGenericParameters("T");

			// Set generic T as return type
			this._unpackMethod.SetReturnType(typeParameters[0]);

			return this;
		}

		/// <summary>
		/// Generates the proxy methods for all public and virtual properties of 
		/// <typeparamref name="T"/>
		/// </summary>
		/// <returns>Self for chaining</returns>
		protected LazyProxyBuilder<T> GeneratePropertyProxies()
		{
			// For each property create a proxy
			foreach (PropertyInfo property in typeof(T).GetProperties())
			{
				if (property.Attributes.HasFlag(MethodAttributes.Final))
				{
					// TODO: Raise warning?
					continue;
				}

				PropertyBuilder propertyBuilder = this._typeBuilder.DefineProperty(
					property.Name,
					property.Attributes,
					property.PropertyType,
					Type.EmptyTypes
				);

				this.GeneratePropertyMethod(true, propertyBuilder, property,
					(il) =>
					{
						// Ensure that the proxied instance is initialized
						il.CallMethod(this._loadMethod);

						// Forward getter call to base implementation (of T)
						il.CallMethod(
							this._loadedInstanceField,
							property.GetGetMethod()
						);
						il.Emit(OpCodes.Ret);
					}
				);

				this.GeneratePropertyMethod(false, propertyBuilder, property,
					(il) =>
					{
						// Ensure that the proxied instance is initialized
						il.CallMethod(this._loadMethod);

						// Forward setter call to base implementation (of T)
						il.CallMethod(
							this._loadedInstanceField,      // Instance
							property.GetSetMethod(),        // Method
							() => il.Emit(OpCodes.Ldarg_1)  // Arguments
						);
						il.Emit(OpCodes.Ret);
					}
				);
			}
			return this;
		}

		/// <summary>
		/// Generates a getter or setter for the provided property information
		/// </summary>
		/// <param name="isGetter">Boolean value indicating whether or not the generated method 
		/// should be a getter</param>
		/// <param name="propertyBuilder">Property builder to create method with</param>
		/// <param name="property">Property to create method for</param>
		/// <param name="methodBody">Body for the method</param>
		protected void GeneratePropertyMethod(bool isGetter, PropertyBuilder propertyBuilder,
			PropertyInfo property, Action<ILGenerator> methodBody)
		{
			MethodInfo baseMethod = isGetter ? property.GetGetMethod() : property.GetSetMethod();
			Type returnType = isGetter ? property.PropertyType : typeof(void);
			Type[] parameters = isGetter ? Type.EmptyTypes : new Type[] { property.PropertyType };

			if (baseMethod == null)
				return;

			// Register method
			this._knownGettersAndSetters.Add(baseMethod);

			// Create method
			MethodBuilder methodBuilder = this._typeBuilder.DefineMethod(
				baseMethod.Name,
				baseMethod.Attributes ^ MethodAttributes.VtableLayoutMask,
				returnType,
				parameters,
				methodBody
			);

			if (property.Attributes.HasFlag(MethodAttributes.Virtual))
			{
				// Register the method as override
				this._typeBuilder.DefineMethodOverride(methodBuilder, baseMethod);
			}

			if (isGetter)
				propertyBuilder.SetGetMethod(methodBuilder);
			else
				propertyBuilder.SetSetMethod(methodBuilder);
		}

		/// <summary>
		/// Generates the proxy methods for all public and virtual methods of 
		/// <typeparamref name="T"/>
		/// </summary>
		/// <returns>Self for chaining</returns>
		protected LazyProxyBuilder<T> GenerateMethodProxies()
		{
			// Methods that are eligible to proxying have to be public, and may not be getters and 
			// setters for properties.
			IEnumerable<MethodInfo> eligible = typeof(T).GetMethods()
				.Where(method =>
					method.DeclaringType == typeof(T) &&
					method.Attributes.HasFlag(MethodAttributes.Public) &&
					!this._knownGettersAndSetters.Contains(method)
				);

			// For each method create a proxy
			foreach (MethodInfo method in eligible)
			{
				if (method.Attributes.HasFlag(MethodAttributes.Final) ||
					!method.Attributes.HasFlag(MethodAttributes.Virtual))
				{
					// TODO: Raise warning?
					continue;
				}

				Type[] parameterTypes = method.GetParameters()
					.Select(parameter => parameter.ParameterType)
					.ToArray();

				MethodBuilder methodBuilder = this._typeBuilder.DefineMethod(
					method.Name,
					method.Attributes ^ MethodAttributes.VtableLayoutMask,
					method.ReturnType,
					parameterTypes,
					il =>
					{
						// Ensure that the proxied instance is initialized
						il.CallMethod(this._loadMethod);

						// Forward invocation to base implementation (of T)
						il.CallMethod(
							this._loadedInstanceField,  // Instance
							method,                     // Method
							() =>
							{
								// Arguments
								for (short index = 0; index < parameterTypes.Length; index++)
									il.Emit(OpCodes.Ldarg, (index + 1));
							}
						);

						il.Emit(OpCodes.Ret);
					}
				);

				// Declare generic type parameters if the method has any
				if (method.IsGenericMethod)
				{
					Type[] typeArguments = method.GetGenericArguments();
					string[] typeNames = new string[typeArguments.Length];

					for (int i = 0; i < typeArguments.Length; i++)
						typeNames[i] = String.Format("T{0}", i.ToString());

					methodBuilder.DefineGenericParameters(typeNames);
				}
			}
			return this;
		}
	}
}
