﻿/*
 * This file contains no code.
 * 
 * The code below is how a generated proxy looks like, created with the following snippet:
 * 
 *		Subject a = LazyProxy.Create(someCallbackMethod);
 *		
 */

/*
using OneOfZero.DotMasquerade.Lazy;
using OneOfZero.DotMasquerade.Sandbox;
using System;
public class OneOfZero_DotMasquerade_Sandbox_Subject__Lazy : Subject, ILazyProxy<Subject>
{
	private object __syncRoot = new object();
	private Func<Subject> __lateCallback;
	private Subject __loadedInstance;
	public OneOfZero_DotMasquerade_Sandbox_Subject__Lazy(Func<Subject> _lateCallback)
	{
		this.__lateCallback = _lateCallback;
	}
	public override void Load()
	{
		lock (this.__syncRoot)
		{
			if (this.__loadedInstance == null)
			{
				this.__loadedInstance = this.__lateCallback();
			}
		}
	}
	public override Subject Unpack()
	{
		this.Load();
		return this.__loadedInstance;
	}
	public override string get_Foo()
	{
		this.Load();
		return this.__loadedInstance.Foo;
	}
	public override void set_Foo(string foo)
	{
		this.Load();
		this.__loadedInstance.Foo = foo;
	}
	public override string get_Bar()
	{
		this.Load();
		return this.__loadedInstance.Bar;
	}
	public override string get_Baz()
	{
		this.Load();
		return this.__loadedInstance.Baz;
	}
	public override void set_Baz(string baz)
	{
		this.Load();
		this.__loadedInstance.Baz = baz;
	}
	public override string Lorem()
	{
		this.Load();
		return this.__loadedInstance.Lorem();
	}
}
*/