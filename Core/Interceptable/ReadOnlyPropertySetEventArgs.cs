﻿using System;
using System.Linq;
using System.Reflection;

namespace OneOfZero.DotMasquerade.Interceptable
{
	/// <summary>
	/// Holds information that is avaialable for the InterceptedPropertySet event.
	/// </summary>
	public class ReadOnlyPropertySetEventArgs : EventArgs
	{
		/// <summary>
		/// Gets the PropertyInfo of the invoked property
		/// </summary>
		public PropertyInfo Property { get; private set; }

		/// <summary>
		/// Gets the name of the invoked property
		/// </summary>
		public string PropertyName
		{
			get { return this.Property.Name; }
		}

		/// <summary>
		/// Gets the value that is to be injected into the property. If the value does not implement
		/// ICloneable, this property will not contain a clone, but a reference to the actual
		/// object.
		/// </summary>
		public object Value { get; private set; }

		/// <summary>
		/// Initializes the event arguments with the required information
		/// </summary>
		/// <param name="property">PropertyInfo pertaining to the invoked property</param>
		/// <param name="value">User-provided value that is going to be set for the property</param>
		public ReadOnlyPropertySetEventArgs(PropertyInfo property, object value)
		{
			this.Property = property;

			if (value.GetType().GetInterfaces().Contains(typeof(ICloneable)))
			{
				this.Value = ((ICloneable)value).Clone();
			}
			else
			{
				this.Value = value;
			}
		}
	}
}
