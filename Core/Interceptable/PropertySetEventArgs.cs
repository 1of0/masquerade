﻿using System;
using System.Reflection;

namespace OneOfZero.DotMasquerade.Interceptable
{
	/// <summary>
	/// Holds information that is avaialable for the InterceptedPropertySet event. Properties with
	/// public setters control the behavior after the event is handeled.
	/// </summary>
	public class PropertySetEventArgs : EventArgs
	{
		/// <summary>
		/// Gets the PropertyInfo of the invoked property
		/// </summary>
		public PropertyInfo Property { get; private set; }

		/// <summary>
		/// Gets the name of the invoked property
		/// </summary>
		public string PropertyName
		{
			get { return this.Property.Name; }
		}

		/// <summary>
		/// Gets or sets the value that is to be injected into the property. Initially this will
		/// contain the user-provided value that is to be set, but it can be modified to your own 
		/// wishes.
		/// </summary>
		public object Value { get; set; }

		/// <summary>
		/// Gets or sets a boolean value that indicates whether or not the interceptee will proceed
		/// normal operation in setting the property value.
		/// </summary>
		public bool Cancel { get; set; }

		/// <summary>
		/// Initializes the event arguments with the required information
		/// </summary>
		/// <param name="property">PropertyInfo pertaining to the invoked property</param>
		/// <param name="value">User-provided value that is going to be set for the property</param>
		public PropertySetEventArgs(PropertyInfo property, object value)
		{
			this.Property = property;
			this.Value = value;
		}
	}
}
