﻿using System;
using System.Reflection;

namespace OneOfZero.DotMasquerade.Interceptable
{
	/// <summary>
	/// Holds information that is avaialable for the InterceptedMethodInvocation event. Properties 
	/// with public setters control the behavior after the event is handeled.
	/// </summary>
	public class MethodInvokeEventArgs : EventArgs
	{
		/// <summary>
		/// Gets the MethodInfo of the invoked method
		/// </summary>
		public MethodInfo Method { get; private set; }

		/// <summary>
		/// Gets the name of the invoked method
		/// </summary>
		public string MethodName
		{
			get { return this.Method.Name; }
		}

		/// <summary>
		/// Gets or sets an array of parameters that are supplied to the invoked method. Initially 
		/// this will contain the user-provided parameters, but it can be modified to your own 
		/// wishes.
		/// </summary>
		public object[] Parameters { get; set; }

		/// <summary>
		/// Gets or sets a boolean value that indicates whether or not the interceptee will proceed
		/// normal operation in invoking the method.
		/// </summary>
		public bool Cancel { get; set; }

		/// <summary>
		/// Initializes the event arguments with the required information
		/// </summary>
		/// <param name="method">MethodInfo pertaining to the invoked method</param>
		/// <param name="parameters">User-provided parameters that are provided to the method
		/// </param>
		public MethodInvokeEventArgs(MethodInfo method, object[] parameters)
		{
			this.Method = method;
			this.Parameters = parameters;
		}
	}
}
