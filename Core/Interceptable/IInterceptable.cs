﻿using System;

namespace OneOfZero.DotMasquerade.Interceptable
{
	interface IInterceptable
	{
		/// <summary>
		/// Fires when a "get" action is invoked on a property
		/// </summary>
		event EventHandler<PropertyGetEventArgs> InterceptedPropertyGet;

		/// <summary>
		/// Fires when a "set" action is invoked on a property
		/// </summary>
		event EventHandler<PropertySetEventArgs> InterceptedPropertySet;

		/// <summary>
		/// Fires when a method is invoked
		/// </summary>
		event EventHandler<MethodInvokeEventArgs> InterceptedMethodInvocation;

		/// <summary>
		/// Fires when an event is invoked
		/// </summary>
		event EventHandler<EventInvokeEventArgs> InterceptedEventInvocation;
	}
}
