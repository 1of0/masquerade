﻿using System;
using System.Reflection;

namespace OneOfZero.DotMasquerade.Interceptable
{
	/// <summary>
	/// Holds information that is avaialable for the InterceptedMethodInvocation event. Properties 
	/// with public setters control the behavior after the event is handeled.
	/// </summary>
	public class EventInvokeEventArgs : EventArgs
	{
		/// <summary>
		/// Gets the EventInfo of the invoked event
		/// </summary>
		public EventInfo Event { get; private set; }

		/// <summary>
		/// Gets the name of the invoked event
		/// </summary>
		public string EventName
		{
			get { return this.Event.Name; }
		}

		/// <summary>
		/// Gets or sets the event arguments that are provided with the event invocation
		/// </summary>
		public EventArgs EventArguments { get; set; }

		/// <summary>
		/// Gets or sets a boolean value that indicates whether or not the interceptee will proceed
		/// normal operation in invoking the event.
		/// </summary>
		public bool Cancel { get; set; }

		/// <summary>
		/// Initializes the event arguments with the required information
		/// </summary>
		/// <param name="eventInfo">EventInfo pertaining to the invoked event</param>
		/// <param name="eventArguments">EventArgs that are provided with the event invocation
		/// </param>
		public EventInvokeEventArgs(EventInfo eventInfo, EventArgs eventArguments)
		{
			this.Event = eventInfo;
			this.EventArguments = eventArguments;
		}
	}
}
