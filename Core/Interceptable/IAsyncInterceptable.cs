﻿using System;

namespace OneOfZero.DotMasquerade.Interceptable
{
	interface IAsyncInterceptable
	{
		/// <summary>
		/// Fires when a "get" action is invoked on a property
		/// </summary>
		event EventHandler<ReadOnlyPropertyGetEventArgs> InterceptedPropertyGet;

		/// <summary>
		/// Fires when a "set" action is invoked on a property
		/// </summary>
		event EventHandler<ReadOnlyPropertySetEventArgs> InterceptedPropertySet;

		/// <summary>
		/// Fires when a method is invoked
		/// </summary>
		event EventHandler<ReadOnlyMethodInvokeEventArgs> InterceptedMethodInvocation;

		/// <summary>
		/// Fires when an event is invoked
		/// </summary>
		event EventHandler<ReadOnlyEventInvokeEventArgs> InterceptedEventInvocation;
	}
}
