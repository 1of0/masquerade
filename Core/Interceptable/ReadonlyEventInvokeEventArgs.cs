﻿using System;
using System.Linq;
using System.Reflection;

namespace OneOfZero.DotMasquerade.Interceptable
{
	/// <summary>
	/// Holds information that is avaialable for the InterceptedMethodInvocation event.
	/// </summary>
	public class ReadOnlyEventInvokeEventArgs : EventArgs
	{
		/// <summary>
		/// Gets the EventInfo of the invoked event
		/// </summary>
		public EventInfo Event { get; private set; }

		/// <summary>
		/// Gets the name of the invoked event
		/// </summary>
		public string EventName
		{
			get { return this.Event.Name; }
		}

		/// <summary>
		/// Gets the event arguments that are provided with the event invocation. If the event 
		/// arguments do not implement ICloneable, this property will not contain a clone, but a 
		/// reference to the actual event arguments.
		/// </summary>
		public EventArgs EventArguments { get; private set; }

		/// <summary>
		/// Initializes the event arguments with the required information
		/// </summary>
		/// <param name="eventInfo">EventInfo pertaining to the invoked event</param>
		/// <param name="eventArguments">EventArgs that are provided with the event invocation
		/// </param>
		public ReadOnlyEventInvokeEventArgs(EventInfo eventInfo, EventArgs eventArguments)
		{
			this.Event = eventInfo;

			if (eventArguments.GetType().GetInterfaces().Contains(typeof(ICloneable)))
			{
				this.EventArguments = (EventArgs)((ICloneable)eventArguments).Clone();
			}
			else
			{
				this.EventArguments = eventArguments;
			}
		}
	}
}
