﻿using System;
using System.Linq;
using System.Reflection;

namespace OneOfZero.DotMasquerade.Interceptable
{
	/// <summary>
	/// Holds information that is avaialable for the InterceptedPropertyGet event.
	/// </summary>
	public class ReadOnlyPropertyGetEventArgs : EventArgs
	{
		/// <summary>
		/// Gets the PropertyInfo of the invoked property
		/// </summary>
		public PropertyInfo Property { get; private set; }

		/// <summary>
		/// Gets the name of the invoked property
		/// </summary>
		public string PropertyName
		{
			get { return this.Property.Name; }
		}

		/// <summary>
		/// Gets the return value that is being returned. If the value does not implement
		/// ICloneable, this property will not contain a clone, but a reference to the actual
		/// object.
		/// </summary>
		public object ReturnValue { get; private set; }

		/// <summary>
		/// Initializes the event arguments with the required information
		/// </summary>
		/// <param name="property">PropertyInfo pertaining to the invoked property</param>
		/// <param name="returnValue">Value that is returned</param>
		public ReadOnlyPropertyGetEventArgs(PropertyInfo property, object returnValue)
		{
			this.Property = property;

			if (returnValue.GetType().GetInterfaces().Contains(typeof(ICloneable)))
			{
				this.ReturnValue = ((ICloneable)returnValue).Clone();
			}
			else
			{
				this.ReturnValue = returnValue;
			}
		}
	}
}
