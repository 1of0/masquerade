﻿using System;
using System.Reflection;

namespace OneOfZero.DotMasquerade.Interceptable
{
	/// <summary>
	/// Holds information that is avaialable for the InterceptedMethodInvocation event.
	/// </summary>
	public class ReadOnlyMethodInvokeEventArgs : EventArgs
	{
		/// <summary>
		/// Gets the MethodInfo of the invoked method
		/// </summary>
		public MethodInfo Method { get; private set; }

		/// <summary>
		/// Gets the name of the invoked method
		/// </summary>
		public string MethodName
		{
			get { return this.Method.Name; }
		}

		/// <summary>
		/// Gets an array of parameters that are supplied to the invoked method. This array is a
		/// copy of the fed array, but array elements hold references to actual parameters.
		/// </summary>
		public object[] Parameters { get; private set; }

		/// <summary>
		/// Initializes the event arguments with the required information
		/// </summary>
		/// <param name="method">MethodInfo pertaining to the invoked method</param>
		/// <param name="parameters">User-provided parameters that are provided to the method
		/// </param>
		public ReadOnlyMethodInvokeEventArgs(MethodInfo method, object[] parameters)
		{
			this.Method = method;

			object[] copy = new object[parameters.Length];
			parameters.CopyTo(copy, 0);
			this.Parameters = copy;
		}
	}
}
