﻿using System;
using System.Reflection;

namespace OneOfZero.DotMasquerade.Interceptable
{
	/// <summary>
	/// Holds information that is avaialable for the InterceptedPropertyGet event. Properties with
	/// public setters control the behavior after the event is handeled.
	/// </summary>
	public class PropertyGetEventArgs : EventArgs
	{
		/// <summary>
		/// Holds a delegate to the property getter on the interceptee
		/// </summary>
		private Func<object> _intercepteeValueDelegate;

		/// <summary>
		/// Holds the return value
		/// </summary>
		private object _returnValue;

		/// <summary>
		/// Gets the PropertyInfo of the invoked property
		/// </summary>
		public PropertyInfo Property { get; private set; }

		/// <summary>
		/// Gets the name of the invoked property
		/// </summary>
		public string PropertyName
		{
			get { return this.Property.Name; }
		}

		/// <summary>
		/// Returns the value that the interceptee (the actual instance) would return
		/// </summary>
		public object IntercepteeValue
		{
			get { return _intercepteeValueDelegate(); }
		}

		/// <summary>
		/// Gets or sets the return value that is to be injected. If the value for this property is
		/// null, and the property Cancel is false, the interceptee value will be set as return
		/// value.
		/// </summary>
		public object ReturnValue
		{
			get
			{
				if (this._returnValue == null && !this.Cancel)
				{
					return this.IntercepteeValue;
				}
				return this._returnValue;
			}
			set
			{
				this._returnValue = value;
			}
		}

		/// <summary>
		/// Gets or sets a boolean value that indicates whether or not the interceptee will proceed
		/// normal operation in returning the property value.
		/// </summary>
		public bool Cancel { get; set; }

		/// <summary>
		/// Initializes the event arguments with the required information
		/// </summary>
		/// <param name="property">PropertyInfo pertaining to the invoked property</param>
		/// <param name="intercepteeValueDelegate">Delegate that should point to the getter method
		/// of the property</param>
		public PropertyGetEventArgs(PropertyInfo property, Func<object> intercepteeValueDelegate)
		{
			this.Property = property;
			this._intercepteeValueDelegate = intercepteeValueDelegate;
		}
	}
}
