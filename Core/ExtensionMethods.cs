﻿using System;
using OneOfZero.DotMasquerade.Lazy;

#if !__MonoCS__
namespace OneOfZero.DotMasquerade
{
	public static class ExtensionMethods
	{
		/// <summary>
		/// If the provided <paramref name="instance"/> is a proxy, this method makes sure that the
		/// underlying object is loaded using the late callback.
		/// </summary>
		/// <typeparam name="T">Proxied type</typeparam>
		/// <param name="instance">Proxy instance</param>
		public static void LazyLoad<T>(this T instance)
		{
			if (LazyProxy.IsProxy(instance))
			{
				(instance as ILazyProxy).Load();
			}
		}

		/// <summary>
		/// If the provided <paramref name="instance"/> is a proxy, this method returns the 
		/// underlying object, so that the proxy may be disposed. If the provided 
		/// <paramref name="instance"/> isn't a proxy, the <paramref name="instance"/> will be 
		/// returned as is.
		/// </summary>
		/// <typeparam name="T">Proxied type</typeparam>
		/// <param name="instance">Proxy instance</param>
		/// <returns>Unproxied instance</returns>
		public static T Unpack<T>(this T instance)
		{
			return LazyProxy.Unpack(instance);
		}

		/// <summary>
		/// Returns a boolean value indicating whether or not the provided 
		/// <paramref name="instance"/> is a proxy.
		/// </summary>
		/// <typeparam name="T">Proxied type</typeparam>
		/// <param name="instance">Proxy instance?</param>
		/// <returns>Wether or not the provided instance is a proxy</returns>
		public static bool IsProxy<T>(this T instance)
		{
			return LazyProxy.IsProxy(instance);
		}
	}
}
#endif
